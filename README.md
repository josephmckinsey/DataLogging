# DataLogging.jl

Ordinary logging uses strings for some reason, with only a few limited exceptions
like tensorboard.

This library provides a `DataLogger` that just serializes special `LogRecord` entries.
We also provide a `StaticHTMLDisplay` with functions to deserialize the records in
a file, so they can be rendered into an HTML display.


## Pros

- You can log plots and tables! This is the most important thing.
- You can hook into existing tools to display HTML or images in data types.
- The log files are dead simple.

They are just a serialized Julia objects with no extra information. In the future we might
move to JSON + serialization in a field or something similar, but in any case, it's
better than digging through tensorboard's ProtocolBuffer interface if you need to
programmatically access data.
- We can take advantage of efforts by IJulia and Pluto.jl to make HTML representations.
- It's easy to just pass an IO stream, so we can enable compression by a one-liner with [`TranscodingStreams.jl`](https://juliaio.github.io/TranscodingStreams.jl/latest/).

## Cons

- You are now tied to Julia's serialization, which is not very stable.
- You must have the right environment
- It's not exactly a drop in replacement since you may want to include `References` to files instead of just the raw data. In addition, most people avoid logging any data with good reason. Most loggers cannot handle logging a plot.
- It's not distributed, even if it is asynchronous. This may affect the memory profile if ou are serializing large objects.
- Some objects will not deserialize correctly.
- You may be serializing some large objects in a single file. This is naturally a little scary.

## TODOs and Developer Notes


- Allow log files to tell what Julia environment to deserialize and show into.
    + Manifest file?
- Allow log files to be split up so it's not one giant file.
    + Not sure the betst way to do this.
- Create web socket based rendering
- Fix some TODOs in code.
- Log message with only a dictionary of entries is very wrong for some reason (see tests).
- Add detail to the docs
- Allow "mutating" logs, where logs accumulate into things like time series. (May require tabs + javascript in the HTML document)
