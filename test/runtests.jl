using Test

@test 1 == 1

using DataLogging
using Logging
using DataLogging.Display
import DataFrames

function basic_logs()
    # Creates a x=3 box and a y=Link to notes
    @warn x=3 y=DataLogging.Reference("test.png", MIME("text/plain"))

    # Should literally show the string <i>
    @info "<i>hello</i>"

    # Should show hello in italics
    @info html"<i>hello</i>"

    # Should show a link to a png.
    @info DataLogging.Reference("winebread.png", MIME("image/png"))

    # Should show a link to a jpeg.
    @info DataLogging.Reference("winebread.jpg", MIME("image/jpeg"))

    # Should show a table in html
    @info DataFrames.DataFrame(x = [2, 3])

    # Should show a table in colored text
    @info DataFrames.DataFrame(x = [2, 3]) _mime=MIME("text/plain")
end

@testset "Type Tests" begin
    if isfile("test.log")
        rm("test.log")
    end
    logger = DataLogger("test.log")
    (@test_logs (:warn,) (:info, "<i>hello</i>") (:info,) (:info,) (:info,) (:info,) (:info,) basic_logs())
end

@testset "Writing Tests" begin
    if isfile("test.log")
        rm("test.log")
    end
    logger = DataLogger("test.log")
    with_logger(basic_logs, logger)
    @test isfile("test.log")
end

@testset "Reading Tests" begin
    io = IOBuffer()
    write(io, displayall(StaticHTMLDisplay("assets"), "test.log"))
    @test 9063 == length(String(take!(io)))
    write("test.html", displayall(StaticHTMLDisplay("assets"), "test.log"))
end

@testset "Failing Reading" begin
    io = IOBuffer()
    @test_logs (:warn, "Unable to read record 5") write(
        io,
        displayall(StaticHTMLDisplay("assets"), "bad.log")
    )
end
