import Logging: LogLevel

struct LogRecord
    level::LogLevel
    message
    _module::Module
    group::Symbol
    id::Symbol
    filepath::String
    line::Int
    kwargs::Dict{Symbol,Any}
end
