module Display

export StaticHTMLDisplay
export displayall

using Base.Multimedia
using Serialization
import Base.display
using HypertextLiteral
import Mustache
import DataLogging: LogRecord
import Logging: LogLevel, Debug, Info, Warn, Error, AboveMaxLevel
import ANSIColoredPrinters: HTMLPrinter

"""
    StaticHTMLDisplay(assetdirectory::String)

A static html display for Julia data types

Unlike ordinary [`AbstractDisplays`](https://docs.julialang.org/en/v1/base/io-network/#Base.Multimedia.AbstractDisplay),
this should not be used as a REPL interface (yet at least). Instead we use this
as a place to record data we've deserialized from a log file.
"""
struct StaticHTMLDisplay <: AbstractDisplay
    assetdirectory::String
    written::Int
    logrecords::Vector{HypertextLiteral.Result}
    @doc "This creates a [StaticHTMLDisplay](@ref) at `assetdirectory`"
    function StaticHTMLDisplay(assetdirectory::String)
        new(assetdirectory, 0, [])
    end
end


const mime_types = map(MIME, [
    "text/html",
    "image/png",
    "image/gif",
    "image/jpeg",
    "text/plain"
])

function preferredmimetype(x)
    for mime in mime_types
        if showable(mime, x)
            return mime
        end
    end
    throw(MethodError(preferredmimetype, (x,)))
end

preferredmimesuffix(::MIME) = return ""
preferredmimesuffix(::MIME"image/png") = return ".png"
preferredmimesuffix(::MIME"image/jpeg") = return ".jpeg"
preferredmimesuffix(::MIME"image/gif") = return ".gif"

displayable(d::StaticHTMLDisplay, M::MIME) = true


render(d::StaticHTMLDisplay, x) = render(d, preferredmimetype(x), x)
function render(d::StaticHTMLDisplay, mimetype::MIME, x)
    io = IOBuffer()
    if istextmime(mimetype)
        if !(mimetype isa MIME"text/html")
            beforeescaping = IOContext(IOBuffer(), :color=>true)
            show(beforeescaping, mimetype, x)
            show(io, MIME("text/html"), HTMLPrinter(beforeescaping.io))
        else
            show(io, mimetype, x)
        end
    else
        open(joinpath(
            d.assetdirectory,
            string(d.written)*preferredmimesuffix(mimetype)
        ), "w") do asset
            show(asset, mimetype, x)
            d.written += 1
        end
        show(io, MIME("text/html"), Reference(filepath, mimetype))
    end
    return HypertextLiteral.Result(HypertextLiteral.Bypass(String(take!(io))))
end

function display(d::StaticHTMLDisplay, x)
    display(d, preferredmimetype(x), x)
end

function colorclass(level::LogLevel)
    if level < Debug
        htl"w3-white"
    elseif level < Info
        htl"w3-light-gray"
    elseif level < Warn
        htl"w3-gray"
    elseif level < Error
        htl"w3-orange"
    elseif level <= AboveMaxLevel
        htl"w3-red"
    else
        htl"w3-xlarge w3-red"
    end
end

render(d::StaticHTMLDisplay, level::LogLevel) = @htl("""<h5 class="$(colorclass(level)) w3-round">$(level)</h5>""")

codeblock(s; header="") = htl"""
<div class="w3-code w3-light-grey">
    <pre><code>
    $(header)
    </code></pre>
    $(s)
</div>"""

function display(d::StaticHTMLDisplay, logrecord::LogRecord)
    if haskey(logrecord.kwargs, :_mime)
        mimetype = logrecord.kwargs[:_mime]
        delete!(logrecord.kwargs, :_mime)
        return display(d, mimetype, logrecord)
    end
    return display(d, preferredmimetype(logrecord.message), logrecord)
end

function display(d::StaticHTMLDisplay, mimetype::MIME, record::LogRecord)
    html = @htl("""<hr>
    <div class="w3-card w3-padding">
        <div class="w3-center w3-container w3-light-blue w3-round w3-row">
            <div class="w3-col m1">
                $(render(d, record.level))
            </div>
            <div class="w3-col m5">
                <h5 class="w3-monospace">$(record.group), $(record._module), $(record.id)</h4>
            </div>
            <div class="w3-col m4">
                <h5 class="w3-monospace">$(record.filepath): $(record.line)</h4>
            </div>
        </div>
        <hr>
        <div class="w3-center w3-container w3-row">
            $(render(d, mimetype, record.message))
            $([codeblock(render(d, value), header=key)
                     for (key, value) in record.kwargs if key != :_mime])
        </div>
    </div>""")
    push!(d.logrecords, html)
end


function renderpage(innerhtml)
    open(joinpath(@__DIR__, "logging.html"), "r") do f
        template = read(f, String)
        return Mustache.render(template, Dict(:html => innerhtml))
    end
end

function displayall(d::StaticHTMLDisplay, io::IO)
    counter = 1
    while !eof(io)
        try
            logrecord = deserialize(io)
            display(d, logrecord)
        catch e
            @warn "Unable to read record $counter"
            break
        end
        counter += 1
    end
    return d
end

displayall(d::StaticHTMLDisplay, filename::AbstractString) = open(filename) do io
    displayall(d, io)
end


Base.write(io::IO, d::StaticHTMLDisplay) = Base.write(io, renderpage(join(d.logrecords)))
Base.write(filename::AbstractString, d::StaticHTMLDisplay) = open(filename, "w") do io
    Base.write(io, d)
end

end
