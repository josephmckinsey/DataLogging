"Creates a link to a reference with a specific MIME type"
struct Reference{T <: MIME}
    filepath::AbstractString
end
Reference(filepath, ::MIME{T}) where T = Reference{MIME{T}}(filepath)

function Base.show(io::IO, ::MIME"text/plain", x::Reference{MIME{T}}) where T
    print(io, "File: ")
    show(io, T)
    println(io)
    print(io, "link: $(x.filepath)")
end

isimagemime(m::MIME) = startswith(string(m), "image/")

"""
Creates an appropriate link tab to the the reference file

If `isimagemime(m)` is true, then it creates an <img> tag,
otherwise it creates an <a> tag.

TODO: This should probably be more extensible and use better dispatching.
"""
function Base.show(io::IO, ::MIME"text/html", x::Reference{T}) where T <: MIME
    if isimagemime(T())
        print(io, """<img src="$(x.filepath)" title="$(x.filepath)"/>""")
    else
        print(io, """<a href="$(x.filepath)" title="$(T)">$(x.filepath)</a>""")
    end
end
