module DataLogging

export DataLogger
export Display
export Reference

using Serialization
using Logging

import Logging: Info,
    shouldlog, handle_message, min_enabled_level, catch_exceptions

include("primitives.jl")
include("renderingtypes.jl")
include("display.jl")

"""
    DataLogger(stream::IO, level=Logging.Info)
    DataLogger(filename::AbstractString, level=Logging.Info)

DataLogger sends serialize Julia data types directly to an IO stream
whenever you log with a level higher than `level`.
"""
struct DataLogger{IOT <: IO} <: AbstractLogger
    "IOStream to save things at"
    file::IOT
    "we save messages with log level higher than `min_level`"
    min_level::LogLevel
    "this allows us to ignore any messages that are logging excessively (see :maxlog)"
    message_limits::Dict{Any,Int}
    "the saver task is used to synchronize sending data to `file``"
    saver::Task
    "the channel is used by the saver to send information"
    channel::Channel
    @doc """
        DataLogger Inner Constructor
    """
    function DataLogger(
        stream::IOT,
        level=Logging.Info,
    ) where IOT <: IO
        c = Channel(Inf)
        task = @async begin
            while isopen(stream)
                serialize(stream, take!(c))
                flush(stream)
            end
        end
        new{IOT}(stream, level, Dict{Any,Int}(), task, c)
    end
end

DataLogger(filename::AbstractString, level=Logging.Info) = DataLogger(open(filename, write=true, append=true), level)

shouldlog(logger::DataLogger, level, _module, group, id) =
    get(logger.message_limits, id, 1) > 0

min_enabled_level(logger::DataLogger) = logger.min_level

catch_exceptions(logger::DataLogger) = false

function handle_message(logger::DataLogger, level::LogLevel, message, _module, group, id,
                        filepath, line; kwargs...)
    @nospecialize
    maxlog = get(kwargs, :maxlog, nothing)
    if maxlog isa Core.BuiltinInts
        remaining = get!(logger.message_limits, id, Int(maxlog)::Int)
        logger.message_limits[id] = remaining - 1
        remaining > 0 || return
    end
    fullrecord = LogRecord(level, message, _module, group, id, filepath, line, kwargs)
    put!(logger.channel, fullrecord)
end

end  # DataLogging
