using Documenter
using DataLogging

makedocs(
    sitename = "DataLogging",
    format = Documenter.HTML(),
    modules = [DataLogging]
)

# Documenter can also automatically deploy documentation to gh-pages.
# See "Hosting Documentation" and deploydocs() in the Documenter manual
# for more information.
#=deploydocs(
    repo = "<repository url>"
)=#
